package neonatal.br.unifor.neonatal.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

import neonatal.br.unifor.neonatal.R;
import neonatal.br.unifor.neonatal.event.DisplayMenuActivityEvent;
import neonatal.br.unifor.neonatal.event.ExitAppEvent;

/**
 * Created by Jonathan on 01/03/2017.
 */

public class DialogUtils  {

    static AlertDialog alertDialog;

    private static int getScreenWidth(Context context) {
        Point size = new Point();
        Activity activity = (Activity) context;
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    private static int getScreenHeight(Context context) {
        Point size = new Point();
        Activity activity = (Activity) context;
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.y;
    }


    public static void createDialogTermoDeUso(final Context context) {

        alertDialog = new AlertDialog.Builder((new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog)))
                .setTitle("Termo de uso")
                .setCancelable(false)
                .setPositiveButton("Continuar", null)
                .setNegativeButton("Cancelar",null)
                .create();

        View input = LayoutInflater.from(context).inflate(R.layout.custom_dialog_termo_de_uso, null);
        final CheckBox checkBox = (CheckBox) input.findViewById(R.id.check_box);
        alertDialog.setView(input);

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialogInterface) {

                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        if (checkBox.isChecked()) {
                            EventBus.getDefault().post(new DisplayMenuActivityEvent());
                            dialogInterface.dismiss();
                        } else {
                            Toast.makeText(context, "É necessário aceitar as regras para continuar.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });


        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));

        alertDialog.getWindow().setLayout((int) (getScreenWidth(context) * .9), (int) (getScreenHeight(context) * .8));

    }

    public static void createDialogSairSistema(final Context context) {
        alertDialog = new AlertDialog.Builder((new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog)))
                .setTitle("Atenção")
                .setMessage("Deseja realmente sair do sistema?")
                .setCancelable(false)
                .setPositiveButton("Confirmar", null)
                .setNegativeButton("Cancelar",null)
                .create();

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialogInterface) {

                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        EventBus.getDefault().post(new ExitAppEvent());
                    }
                });
            }
        });
        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));
    }

    public static void createDialogRazaoTransmistir(final Context context) {

        alertDialog = new AlertDialog.Builder((new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog)))
                .setTitle("Razão para não transmitir")
                .setCancelable(false)
                .setPositiveButton("Continuar", null)
                .setNegativeButton("Cancelar",null)
                .create();

        View input = LayoutInflater.from(context).inflate(R.layout.custom_dialog_razao_transmitir, null);

        final RadioGroup radiog1 = (RadioGroup) input.findViewById(R.id.radiog1);

        final RadioGroup radiog2 = (RadioGroup) input.findViewById(R.id.radiog2);

        final RadioButton radioButton = (RadioButton) input.findViewById(R.id.radio8);

        final RadioGroup.OnCheckedChangeListener listener = new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (group.getId() == R.id.radiog1) {
                    radiog2.setOnCheckedChangeListener(null);
                    radiog2.clearCheck();
                    radiog2.setOnCheckedChangeListener(this);
                } else {
                    radiog1.setOnCheckedChangeListener(null);
                    radiog1.clearCheck();
                    radiog1.setOnCheckedChangeListener(this);
                }

                radioButton.setChecked(false);
            }
        };

        radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (radioButton.isChecked()) {
                    radiog1.setOnCheckedChangeListener(null);
                    radiog2.setOnCheckedChangeListener(null);
                    radiog1.clearCheck();
                    radiog2.clearCheck();
                    radiog1.setOnCheckedChangeListener(listener);
                    radiog2.setOnCheckedChangeListener(listener);
                }
            }
        });

        radiog1.setOnCheckedChangeListener(listener);
        radiog2.setOnCheckedChangeListener(listener);

        alertDialog.setView(input);

        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialogInterface) {

                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                    }
                });

            }
        });

        alertDialog.show();
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));
        alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_light));

        alertDialog.getWindow().setLayout((int) (getScreenWidth(context) * .9), (int) (getScreenHeight(context) * .8));
    }

}
