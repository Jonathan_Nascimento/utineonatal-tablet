package neonatal.br.unifor.neonatal.customItens;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import neonatal.br.unifor.neonatal.R;
import neonatal.br.unifor.neonatal.event.ClickCardViewRNEvent;

/**
 * Created by Jonathan on 09/03/2017.
 */

@EViewGroup(R.layout.item_view_choose_rn)
public class RNItemView extends RelativeLayout{

    @ViewById
    TextView nomeMae;

    public void bind(int position) {

    }

    public RNItemView(Context context) {
        super(context);
    }

    @Click(R.id.card_view)
    public void itemClick() {
        EventBus.getDefault().post(new ClickCardViewRNEvent());
    }

}
