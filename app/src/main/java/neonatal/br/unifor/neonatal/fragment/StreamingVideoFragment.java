package neonatal.br.unifor.neonatal.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioGroup;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import neonatal.br.unifor.neonatal.R;
import neonatal.br.unifor.neonatal.streaming.StreamingActivity_;
import neonatal.br.unifor.neonatal.utils.DialogUtils;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_streaming_video)
public class StreamingVideoFragment extends Fragment {

    @Click
    public void rejectStreaming() {
        DialogUtils.createDialogRazaoTransmistir(getActivity());
    }

    @Click
    public void allowStreaming() {
        StreamingActivity_.intent(this).start();
    }


}
