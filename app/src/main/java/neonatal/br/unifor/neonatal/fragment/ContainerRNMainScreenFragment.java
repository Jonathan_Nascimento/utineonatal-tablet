package neonatal.br.unifor.neonatal.fragment;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.PageSelected;
import org.androidannotations.annotations.ViewById;

import neonatal.br.unifor.neonatal.R;
import neonatal.br.unifor.neonatal.activity.ContainerMenuActivity;
import neonatal.br.unifor.neonatal.adapter.ContainerMainScreenAdapter;


@EFragment(R.layout.fragment_container_rn_main_screen)
public class ContainerRNMainScreenFragment extends BaseFragment<ContainerMenuActivity> {

    @ViewById
    ViewPager viewPager;

    ContainerMainScreenAdapter adapter;

    @AfterViews
    public void afterViews() {
        adapter = new ContainerMainScreenAdapter(activity.getSupportFragmentManager());
        activity.getTabLayout().setupWithViewPager(viewPager);
        viewPager.setAdapter(adapter);
    }
}
