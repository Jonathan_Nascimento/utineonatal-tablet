package neonatal.br.unifor.neonatal.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidadvance.topsnackbar.TSnackbar;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.Subscribe;

import neonatal.br.unifor.neonatal.R;
import neonatal.br.unifor.neonatal.event.DisplayMenuActivityEvent;
import neonatal.br.unifor.neonatal.utils.DialogUtils;

@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity {

    @ViewById
    CoordinatorLayout coordinatorLayout;

    @ViewById
    View keyboard;

    @AfterViews
    public void afterViews() {
        showSnackBar("DEU CERTO!");
    }

    @Click(R.id.senha)
    public void clickEditSenha() {
        keyboard.setVisibility(View.VISIBLE);
    }

    @Click(R.id.n_coren)
    public void clickNCoren() {
        keyboard.setVisibility(View.VISIBLE);
    }

    @Click({R.id.key_0, R.id.key_2, R.id.key_1, R.id.key_ok})
    public void clickKeyboard(View view) {

        String tag = (String) view.getTag();

        if (tag.equalsIgnoreCase("del")) {

        } else if (tag.equalsIgnoreCase("ok")) {
            keyboard.setVisibility(View.INVISIBLE);
        } else {

        }
    }

    @Subscribe
    public void onReceiveEventClickTermoDeUsoDialog(DisplayMenuActivityEvent event) {
        ContainerMenuActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
    }


    @Click
    public void entrar() {
        DialogUtils.createDialogTermoDeUso(this);
    }
}
