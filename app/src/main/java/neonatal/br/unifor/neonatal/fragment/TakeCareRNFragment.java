package neonatal.br.unifor.neonatal.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EFragment;

import neonatal.br.unifor.neonatal.R;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_take_care_rn)
public class TakeCareRNFragment extends Fragment {

}
