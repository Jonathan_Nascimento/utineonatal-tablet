package neonatal.br.unifor.neonatal.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import neonatal.br.unifor.neonatal.fragment.ChoiceRNFragment;
import neonatal.br.unifor.neonatal.fragment.ChoiceRNFragment_;
import neonatal.br.unifor.neonatal.fragment.ContainerChoiceRNFragment;

/**
 * Created by Jonathan on 08/03/2017.
 */

public class ContainerSlidindRNAdapter extends FragmentStatePagerAdapter {

    private int nOfTabs;

    public ContainerSlidindRNAdapter(FragmentManager fm, int nOfTabs) {
        super(fm);
        this.nOfTabs = nOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
       ChoiceRNFragment choiceRNFragment = ChoiceRNFragment_.builder().build();
       return choiceRNFragment;
    }

    @Override
    public int getCount() {
      return nOfTabs;
    }
}
