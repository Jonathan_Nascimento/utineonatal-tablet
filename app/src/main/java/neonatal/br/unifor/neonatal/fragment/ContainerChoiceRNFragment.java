package neonatal.br.unifor.neonatal.fragment;

import android.media.Image;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.PageSelected;
import org.androidannotations.annotations.ViewById;

import neonatal.br.unifor.neonatal.R;
import neonatal.br.unifor.neonatal.adapter.ContainerSlidindRNAdapter;


@EFragment(R.layout.fragment_container_choice_rn)
public class ContainerChoiceRNFragment extends Fragment {

    @ViewById
    LinearLayout counterDots;

    @ViewById
    ViewPager viewPager;

    ContainerSlidindRNAdapter adapter;

    @AfterViews
    public void afterViews() {

        viewPager.setOffscreenPageLimit(1);

        adapter = new ContainerSlidindRNAdapter(getActivity().getSupportFragmentManager(), 6);
        viewPager.setAdapter(adapter);

        setUIPageDotsIndicator();
    }

    private void setUIPageDotsIndicator() {

        for (int i = 0; i <= 5; i++) {

            ImageView image = new ImageView(getActivity());

            if (i == 0) {
                image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.icon_ball_fill));
            } else {
                image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.icon_ball_not_fill));
            }

            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

            lp.setMargins(0,0,30,0);

            image.setLayoutParams(lp);

            counterDots.addView(image);
        }
    }

    @PageSelected(R.id.view_pager)
    void onPageSelected(ViewPager view, int state) {
        adjustBallsInditicator(state);
    }


    private void adjustBallsInditicator(int position) {

        for (int i = 0 ; i <=5;i++) {

            ImageView image = (ImageView) counterDots.getChildAt(i);

            if (i == position) {
                image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.icon_ball_fill));
            } else {
                image.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.icon_ball_not_fill));
            }
        }
    }
}
