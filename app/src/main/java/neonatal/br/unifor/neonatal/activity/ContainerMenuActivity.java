package neonatal.br.unifor.neonatal.activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.Subscribe;

import neonatal.br.unifor.neonatal.R;
import neonatal.br.unifor.neonatal.event.ExitAppEvent;
import neonatal.br.unifor.neonatal.fragment.ContainerChoiceRNFragment_;
import neonatal.br.unifor.neonatal.fragment.ContainerRNMainScreenFragment_;
import neonatal.br.unifor.neonatal.utils.DialogUtils;

@EActivity(R.layout.activity_container_menu)
public class ContainerMenuActivity extends BaseActivity {

    @ViewById
    Toolbar toolbar;

    @ViewById
    TabLayout tabLayout;

    @ViewById
    View horizontalView;

    @ViewById
    CardView cardView;

    @ViewById
    RelativeLayout trocarRn;

    @AfterViews
    public void afterViews() {

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        swapChoiceRNFragment();
    }
    public TabLayout getTabLayout() {
        return tabLayout;
    }

    private void configureMenuViews() {
        tabLayout.setVisibility(View.VISIBLE);
        horizontalView.setVisibility(View.VISIBLE);
        trocarRn.setVisibility(View.VISIBLE);
    }

   public void swapChoiceRNFragment() {

       configureChoiceRnViews();

       Fragment fragment = ContainerChoiceRNFragment_.builder().build();
       getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
   }

    private void configureChoiceRnViews() {
        tabLayout.setVisibility(View.GONE);
        horizontalView.setVisibility(View.INVISIBLE);
        trocarRn.setVisibility(View.INVISIBLE);
    }

    public void swapRNMainScreenFragment() {

        configureMenuViews();

        Fragment fragment = ContainerRNMainScreenFragment_.builder().build();
        getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment).commit();
    }

    @Subscribe
    public void exitAppEvent(ExitAppEvent event) {
        LoginActivity_.intent(this).flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK).start();
    }

    @Click
    public void trocarRn() {
        swapChoiceRNFragment();
    }

    @Click
    public void sair() {
        DialogUtils.createDialogSairSistema(this);
    }

    @Click
    public void sobre() {

    }

}
