package neonatal.br.unifor.neonatal.adapter;

import android.content.Context;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import neonatal.br.unifor.neonatal.customItens.RNItemView;
import neonatal.br.unifor.neonatal.customItens.RNItemView_;
import neonatal.br.unifor.neonatal.utils.ViewWrapper;

@EBean
public class ChooseRNAdapter  extends RecyclerViewAdapterBase<String, RNItemView> {

    @RootContext
    Context context;

    @AfterInject
    public void afterInject() {
        items.add("sdsds");
        items.add("sdsds");
        items.add("sdsds");
        items.add("sdsds");
        items.add("sdsds");
    }

    @Override
    protected RNItemView onCreateItemView(ViewGroup parent, int viewType) {
        return RNItemView_.build(context);
    }

    @Override
    public void onBindViewHolder(ViewWrapper<RNItemView> holder, int position) {
        holder.getView().bind(position);
    }
}
