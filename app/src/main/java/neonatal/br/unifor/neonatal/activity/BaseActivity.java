package neonatal.br.unifor.neonatal.activity;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ContentFrameLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Jonathan on 27/02/2017.
 */

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Subscribe
    public void defaultSubscribe(Object ignored) {}

    public void showSnackBar(String message) {

        View rootView = this.getWindow().getDecorView().findViewById(android.R.id.content);

        Snackbar snackbar = Snackbar.make(rootView, message, Snackbar.LENGTH_LONG);

        View view = snackbar.getView();

        ContentFrameLayout.LayoutParams params = (ContentFrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP;
        params.width = params.MATCH_PARENT;

        TextView mTextView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        mTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        mTextView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        view.setLayoutParams(params);

        view.setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_red_dark));

        snackbar.show();
    }
}
