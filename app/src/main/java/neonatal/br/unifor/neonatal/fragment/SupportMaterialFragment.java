package neonatal.br.unifor.neonatal.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import neonatal.br.unifor.neonatal.R;

/**
 * A simple {@link Fragment} subclass.
 */
@EFragment(R.layout.fragment_take_care_rn)
public class SupportMaterialFragment extends Fragment {


    @ViewById
    TextView lastViewedBoasPraticas;

    @ViewById
    TextView lastViewedColeta;

    @ViewById
    TextView lastViewedSuporte;

    @ViewById
    TextView lastViewedTermo;


    @AfterViews
    public void afterViews() {
        lastViewedBoasPraticas.setVisibility(View.INVISIBLE);
        lastViewedColeta.setVisibility(View.INVISIBLE);
        lastViewedSuporte.setVisibility(View.INVISIBLE);
        lastViewedTermo.setVisibility(View.INVISIBLE);
    }

}
