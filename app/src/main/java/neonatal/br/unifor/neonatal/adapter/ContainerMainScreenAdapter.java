package neonatal.br.unifor.neonatal.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import neonatal.br.unifor.neonatal.fragment.ContainerChoiceRNFragment;
import neonatal.br.unifor.neonatal.fragment.StreamingVideoFragment_;
import neonatal.br.unifor.neonatal.fragment.SupportMaterialFragment_;
import neonatal.br.unifor.neonatal.fragment.TakeCareRNFragment_;

public class ContainerMainScreenAdapter extends FragmentStatePagerAdapter {

    private String tabTitles[] = new String[]{"Transmissão em vídeo", "Cuidado ao RN", "Material de Apoio"};

    public ContainerMainScreenAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return StreamingVideoFragment_.builder().build();
            case 1:
                return TakeCareRNFragment_.builder().build();
            case 2:
                return SupportMaterialFragment_.builder().build();
            default:
                return new Fragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }
}
