package neonatal.br.unifor.neonatal.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.Subscribe;

import neonatal.br.unifor.neonatal.R;
import neonatal.br.unifor.neonatal.activity.ContainerMenuActivity;
import neonatal.br.unifor.neonatal.adapter.ChooseRNAdapter;
import neonatal.br.unifor.neonatal.event.ClickCardViewRNEvent;

@EFragment(R.layout.fragment_choice_rn)
public class ChoiceRNFragment extends BaseFragment<ContainerMenuActivity> {

    @ViewById
    RecyclerView recyclerView;

    @Bean
    ChooseRNAdapter adapter;

    @AfterViews
    public void afterViews() {
        recyclerView.setAdapter(adapter);
    }

    boolean callOneTime = false;

    @Subscribe
    public void ItemViewClick(ClickCardViewRNEvent event) {
        if (!callOneTime) {
            activity.swapRNMainScreenFragment();
            callOneTime = true;
        }
    }
}
